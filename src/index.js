import { createHash } from 'node:crypto';
import { readdirSync, lstatSync, readFileSync, existsSync } from 'node:fs';
import { join, relative } from 'node:path';

// Helper function to hash a content
function hashContent(content, algorithm = 'sha256') {
  const hashSum = createHash(algorithm);
  hashSum.update(content);
  return hashSum.digest('hex');
}

// Helper function to calculate checksum from a directory
// Files in directory are sorted alphabetically and added to the checksum so alphabetical order is preserved
function calculateChecksumFromDir(directory, algorithm = 'sha256', inputs = [], baseDir = directory) {
  const dirFiles = readdirSync(directory);

  for (const file of dirFiles) {
    const filePath = join(directory, file);
    if (lstatSync(filePath).isFile()) {
      const fileContent = readFileSync(filePath);
      inputs.push({
        path: relative(baseDir, filePath),
        hash: hashContent(fileContent, algorithm)
      });
    } else if (lstatSync(filePath).isDirectory()) {
      calculateChecksumFromDir(filePath, algorithm, inputs, baseDir);
    }
  }

  inputs.sort((a, b) => a.path.localeCompare(b.path));

  const hashSum = createHash(algorithm);
  for (const input of inputs) {
    hashSum.update(input.hash);
  }

  return {
    algorithm,
    value: hashSum.digest('hex'),
    inputs: inputs.map(i => i.path)
  };
}

// Helper function to calculate checksum from an object
// It does support nested objects!
// It does support just string or Buffer values!
function calculateChecksumFromObject(inputsObject, algorithm = 'sha256') {
  const inputPaths = Object.keys(inputsObject).sort();
  let inputsArray = [];

  const hash = createHash(algorithm);

  for (let inputPath of inputPaths) {
    let inputContent = inputsObject[inputPath];
    const inputHash = hashContent(inputContent, algorithm);
    hash.update(inputHash);

    inputsArray.push(inputPath);
  }

  return {
    algorithm,
    value: hash.digest('hex'),
    inputs: inputsArray
  };
}

function calculateChecksumFromArray(inputObjects, algorithm = 'sha256') {

  const hash = createHash(algorithm);

  inputObjects.forEach(input => {
    const inputHash = hashContent(input, algorithm);
    hash.update(inputHash);
  });

  return {
    algorithm,
    value: hash.digest('hex')
  };
}

export default async ({ content, path, algorithm = 'sha256' }) => {

  if (content) {
    if (typeof content === 'string' || content instanceof Buffer) {
      const value = hashContent(content, algorithm);
      return {
        algorithm,
        value,
      };
    }
    else if (Array.isArray(content))
      return calculateChecksumFromArray(content, algorithm);
    else if (typeof content === 'object')
      return calculateChecksumFromObject(content, algorithm);
    else
      throw new Error('Invalid content type for calculateChecksum. Expected a string, Buffer, or object.');
  }

  if (path) {
    if (existsSync(path)) {
      if (lstatSync(path).isFile()) {
        // If path is a file, calculate checksum for the file
        const fileContent = readFileSync(path);
        const value = hashContent(fileContent, algorithm);
        return {
          algorithm,
          value,
          inputs: [path]
        };
      } else if (lstatSync(path).isDirectory()) {
        // If path is a directory, calculate checksum for the directory
        return calculateChecksumFromDir(path, algorithm);
      } else {
        throw new Error('Invalid path type for calculateChecksum. Path is neither a file nor a directory.');
      }
    } else {
      throw new Error('Invalid path for calculateChecksum. Path does not exist.');
    }
  } else {
    throw new Error('Invalid input for calculateChecksum. Provide content or path.');
  }
}