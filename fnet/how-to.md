# Developer Guide for `@fnet/checksum` Library

## Overview

The `@fnet/checksum` library is designed to help developers compute checksums for various types of data, ensuring data integrity and reliability. It provides functions to generate checksums for files, directories, and data objects using configurable hashing algorithms. This can be particularly useful for verifying data consistency during file transfers, backups, or data storage.

## Installation

To add `@fnet/checksum` to your project, use either npm or yarn:

```bash
npm install @fnet/checksum
```

or

```bash
yarn add @fnet/checksum
```

## Usage

The library exports a core function that allows you to compute checksums. Here's how you can use it in different scenarios:

```javascript
import calculateChecksum from '@fnet/checksum';

// Example: Calculate checksum for a string or Buffer content
calculateChecksum({ content: 'Sample data' }).then(checksum => {
  console.log(checksum);
});

// Example: Calculate checksum for files
calculateChecksum({ path: '/path/to/file.txt' }).then(checksum => {
  console.log(checksum);
});

// Example: Calculate checksum for directories
calculateChecksum({ path: '/path/to/directory' }).then(checksum => {
  console.log(checksum);
});

// Example: Calculate checksum for an object
calculateChecksum({ content: { 'file1': 'data1', 'file2': 'data2' } }).then(checksum => {
  console.log(checksum);
});
```

## Examples

### Calculate Checksum for Simple Text

```javascript
calculateChecksum({ content: 'Hello World' }).then(checksum => {
  console.log('Checksum for text:', checksum.value);
});
```

### Calculate Checksum for a File

```javascript
calculateChecksum({ path: '/path/to/myfile.txt' }).then(checksum => {
  console.log('Checksum for file:', checksum.value);
});
```

### Calculate Checksum for a Directory

```javascript
calculateChecksum({ path: '/path/to/mydir' }).then(checksum => {
  console.log('Checksum for directory:', checksum.value);
  console.log('Files included in checksum:', checksum.inputs);
});
```

### Calculate Checksum for an Object

```javascript
const dataObject = {
  'file1.txt': 'This is content of file1',
  'file2.txt': 'This is content of file2'
};

calculateChecksum({ content: dataObject }).then(checksum => {
  console.log('Checksum for object:', checksum.value);
});
```

## Acknowledgement

The `@fnet/checksum` library utilizes Node.js built-in modules for filesystem operations and cryptographic processes. Special thanks to contributors and the open-source community for making such tools accessible and reliable.