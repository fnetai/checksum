# @fnet/checksum

This project offers a straightforward tool for generating checksums, which are useful for verifying the integrity of files, directories, or data structures. It provides a reliable method to ensure that the contents of your data remain consistent and unchanged during transfer or storage. 

## How It Works

The tool computes a checksum, a form of a digital fingerprint, using a specified hashing algorithm (defaulted to SHA-256) for various types of inputs. Whether you want to validate the integrity of a single file, a whole directory, or even a complex object structure, this utility offers a simple and effective way to calculate these checksums.

## Key Features

- **File Checksum**: Compute the checksum of a single file to ensure its integrity.
- **Directory Checksum**: Calculate a checksum for all files within a directory, maintaining an alphabetical order for consistency.
- **Object and Array Checksum**: Supports generating checksums for strings, buffers, nested objects, and arrays, giving flexible support for various data structures.

## Conclusion

In summary, @fnet/checksum provides a dependable way to compute checksums for files, directories, and various data structures. It aids users in confirming the data integrity in a straightforward and efficient manner.