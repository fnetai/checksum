import Node from "../src"
import path from "path";

export default async ({ argv }) => {

    const stringTest = await Node({ content: "Some thing" });
    console.log(stringTest);

    const objectTest = await Node({ content: { some: "thing" } });
    console.log(objectTest);

    const fileTest = await Node({ path: path.resolve(process.cwd(), "../cli/index.js") });
    console.log(fileTest);

    const dirTest = await Node({ path: path.resolve(process.cwd(), "../cli") });
    console.log(dirTest);
}